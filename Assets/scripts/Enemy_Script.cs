using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Script : MonoBehaviour
{
    private Animator _animator;
    private SpriteRenderer sr;
    private Rigidbody2D rb2d;

    private float Vida = 3;
    public float RunSpeed;
    public float TimeT;
    private float Runtime = 0f;

    public Pj_Script pj_Script;

    void Start()
    {
        sr = GetComponent<SpriteRenderer>();
        _animator = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();

    }
    void Update()
    {
        //Debug.Log("vida: " + Vida);
         if (Vida <= 0)
        {
            pj_Script.n_enemigos--;
                Destroy(this.gameObject);
        }else
        {
            Runtime += Time.deltaTime;
            if (Runtime < TimeT)
            {
                sr.flipX = false;
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);
            }
            if (Runtime > TimeT)
            {
                sr.flipX = true;
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);
            }
            if (Runtime >= TimeT * 2)
                Runtime = 0f;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision2D)
    {

        if (collision2D.gameObject.layer == 6)
        {
            Vida--;
        }
        if (collision2D.gameObject.layer == 7)
        {
            Vida -=2;
        }
    }
}
