using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Pj_Script_Escena_2 : MonoBehaviour
{
    private Rigidbody2D rb2d;
    private SpriteRenderer sr;
    private Transform _transform;
    private Animator _animator;
    private Color originalColor;

    public GameObject Bala_P_D;
    public GameObject Bala_P_I;

    public GameObject Bala_G_D;
    public GameObject Bala_G_I;

    private const float Bala_G_T = 2;
    private float Shoot_time = 0;

    private float switchColorTime = 0f;
    private float switchColorDelay = 0.1f;

    private bool EstaTocandoElSuelo = false;
    public int upSpeed = 60;
    public int RunSpeed = 15;
    public int Vida = 3;
    public int n_enemigos = 5;

    public float TimeT = 0f;
    private float Runtime = 30f;


    public Text N_Enemigos;
    public Text GameOver;
    public Text Press;
    public Text Text_time;
    //private bool muerto = false;

    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        sr = GetComponent<SpriteRenderer>();
        _transform = GetComponent<Transform>();
        _animator = GetComponent<Animator>();
        originalColor = sr.color;
    }

    void Update()
    {
        Runtime -= Time.deltaTime;
        Debug.Log(Runtime);
        if (Runtime <= TimeT)
        {
            Vida = 0;
        }


        if (Vida > 0)
        {
            N_Enemigos.text = "X" + n_enemigos;
            Text_time.text = "" + Runtime;
            if (Input.GetKey(KeyCode.RightArrow))
            {
                sr.flipX = false;
                setCorrerAnimation();
                rb2d.velocity = new Vector2(RunSpeed, rb2d.velocity.y);

            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                sr.flipX = true;
                setCorrerAnimation();
                rb2d.velocity = new Vector2(-RunSpeed, rb2d.velocity.y);

            }
            else
            {
                setIdleAnimation();
                rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            }
            if (Input.GetKeyDown(KeyCode.Space) && EstaTocandoElSuelo)
            {
                setJumpAnimation();
                rb2d.velocity = Vector2.up * upSpeed;
                EstaTocandoElSuelo = false;
            }
            if (Input.GetKey(KeyCode.Z))
            {
                if (!sr.flipX)
                {
                    setSlideAnimation();
                    rb2d.velocity = new Vector2(RunSpeed + 10, rb2d.velocity.y);
                }
                else
                {
                    setSlideAnimation();
                    rb2d.velocity = new Vector2(-RunSpeed - 10, rb2d.velocity.y);
                }
            }
            if (Input.GetKey(KeyCode.X))
            {

                Shoot_time += Time.deltaTime;
                if (Shoot_time > 0.1)
                    Parpadear();


            }
            if (Input.GetKeyUp("x"))
            {
                if (Shoot_time < Bala_G_T)
                {
                    if (sr.flipX)
                    {
                        setRun_ShootAnimation();
                        var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
                        Instantiate(Bala_P_I, BulletPosition, Quaternion.identity);
                    }
                    else
                    {
                        setRun_ShootAnimation();
                        var BulletPosition = new Vector3(_transform.position.x + 1f, _transform.position.y, _transform.position.z);
                        Instantiate(Bala_P_D, BulletPosition, Quaternion.identity);
                    }

                }
                else
                {
                    if (sr.flipX)
                    {
                        setRun_ShootAnimation();
                        var BulletPosition = new Vector3(_transform.position.x - 1f, _transform.position.y, _transform.position.z);
                        Instantiate(Bala_G_I, BulletPosition, Quaternion.identity);
                    }
                    else
                    {
                        setRun_ShootAnimation();
                        var BulletPosition = new Vector3(_transform.position.x + 1f, _transform.position.y, _transform.position.z);
                        Instantiate(Bala_G_D, BulletPosition, Quaternion.identity);
                    }

                }

                Shoot_time = 0;
                sr.color = originalColor;
            }

        }
        else
        {
            setDeadAnimation();
            rb2d.velocity = new Vector2(0, rb2d.velocity.y);
            GameOver.gameObject.SetActive(true);
            Press.gameObject.SetActive(true);
            if (Input.GetKeyDown(KeyCode.Space))
            {
                GameOver.gameObject.SetActive(false);
                Press.gameObject.SetActive(false);
                RestarGame();
            }
        }
    }
    private void Parpadear()
    {
        switchColorTime += Time.deltaTime;
        if (switchColorTime > switchColorDelay)
        {
            if (sr.color == originalColor)
                sr.color = Color.red;
            else
                sr.color = originalColor;
            switchColorTime = 0;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        EstaTocandoElSuelo = true;

        if (collision.gameObject.layer == 3)
        {
            Vida--;
            Destroy(collision.gameObject);
        }
        if (collision.gameObject.layer == 8)
            GameOver.gameObject.SetActive(true);
            Text_time.gameObject.SetActive(false);

    }
    public void RestarGame()
    {
        SceneManager.LoadScene("Scene_2");
    }
    private void setIdleAnimation()
    {
        _animator.SetInteger("Estado", 0);
    }
    private void setCorrerAnimation()
    {
        _animator.SetInteger("Estado", 1);
    }
    private void setJumpAnimation()
    {
        _animator.SetInteger("Estado", 2);
    }
    private void setSlideAnimation()
    {
        _animator.SetInteger("Estado", 3);
    }
    private void setShootAnimation()
    {
        _animator.SetInteger("Estado", 4);
    }
    private void setRun_ShootAnimation()
    {
        _animator.SetInteger("Estado", 5);
    }
    private void setDeadAnimation()
    {
        _animator.SetInteger("Estado", 6);
    }
}

